{
    "name": "Odhin Theme",
    "author": "Rodrigo Odhin",
    "variables":
    {
        "color1":  "#F5F3EF",
        "color2":  "#A9DEF9",
        "color3":  "#6280A3",
        "color4":  "#647382",
        "color5":  "#272D38",
        "color6":  "#D2D9E6",
        "color7":  "#BE95C4",
        "color8":  "#E1C340",
        "color9":  "#8CB369",
        "color10": "#E5989B",
        "color11": "#F18500",
        "color12": "#D19657"
    },
    "globals":
    {
        "foreground": "var(color6)",
        "background": "var(color5)",
        "accent": "var(color2)",
        "caret": "var(color10)",
        "line_highlight": "var(color2)",
        "selection": "var(color2)",
        "selection_border": "var(color4)",
        "inactive_selection": "var(color2)",
        "misspelling": "var(color10)",
        "shadow": "color(var(color1) alpha(0.25))",
        "active_guide": "var(color8)",
        "stack_guide": "color(var(color8) alpha(0.5))",
        "highlight": "var(color8)",
        "find_highlight_foreground": "var(color6)",
        "find_highlight": "var(color11)",
        "brackets_options": "underline",
        "brackets_foreground": "var(color10)",
        "bracket_contents_options": "underline",
        "bracket_contents_foreground": "var(color8)",
        "tags_options": "stippled_underline",
        "tags_foreground": "var(color7)"
    },
    "rules":
    [
        {
            "name": "Comment",
            "scope": "comment, punctuation.definition.comment",
            "foreground": "var(color3)"
        },
        {
            "name": "String",
            "scope": "string",
            "foreground": "var(color9)"
        },
        {
            "name": "Punctuation",
            "scope": "punctuation.definition - punctuation.definition.numeric.base",
            "foreground": "var(color8)"
        },
        {
            "name": "Number",
            "scope": "constant.numeric",
            "foreground": "var(color10)"
        },
        {
            "name": "Number Suffix",
            "scope": "storage.type.numeric",
            "foreground": "var(color7)",
            "font_style": "italic"
        },
        {
            "name": "Built-in constant",
            "scope": "constant.language",
            "foreground": "var(color10)",
            "font_style": "italic"
        },
        {
            "name": "User-defined constant",
            "scope": "constant.character, constant.other",
            "foreground": "var(color7)"
        },
        {
            "name": "Member Variable",
            "scope": "variable.member",
            "foreground": "var(color10)"
        },
        {
            "name": "Keyword",
            "scope": "keyword - keyword.operator, keyword.operator.word",
            "foreground": "var(color7)"
        },
        {
            "name": "Operators",
            "scope": "keyword.operator",
            "foreground": "var(color7)"
        },
        {
            "name": "Punctuation",
            "scope": "punctuation.separator, punctuation.terminator",
            "foreground": "var(color1)"
        },
        {
            "name": "Punctuation",
            "scope": "punctuation.section",
            "foreground": "var(color1)"
        },
        {
            "name": "Accessor",
            "scope": "punctuation.accessor",
            "foreground": "var(color3)"
        },
        {
            "name": "Annotation Punctuation",
            "scope": "punctuation.definition.annotation",
            "foreground": "var(color8)"
        },
        {
            "name": "JavaScript Dollar",
            "scope": "variable.other.dollar.only.js, variable.other.object.dollar.only.js, variable.type.dollar.only.js, support.class.dollar.only.js",
            "foreground": "var(color8)"
        },
        {
            "name": "Storage",
            "scope": "storage",
            "foreground": "var(color10)"
        },
        {
            "name": "Storage type",
            "scope": "storage.type",
            "foreground": "var(color7)",
            "font_style": "italic"
        },
        {
            "name": "Entity name",
            "scope": "entity.name.function",
            "foreground": "var(color8)"
        },
        {
            "name": "Entity name",
            "scope": "entity.name - (entity.name.section | entity.name.tag | entity.name.label)",
            "foreground": "var(color1)"
        },
        {
            "name": "Inherited class",
            "scope": "entity.other.inherited-class",
            "foreground": "var(color8)",
            "font_style": "italic"
        },
        {
            "name": "Function argument",
            "scope": "variable.parameter",
            "foreground": "var(color10)"
        },
        {
            "name": "Language variable",
            "scope": "variable.language",
            "foreground": "var(color10)",
            "font_style": "italic"
        },
        {
            "name": "Tag name",
            "scope": "entity.name.tag",
            "foreground": "var(color7)"
        },
        {
            "name": "Tag attribute",
            "scope": "entity.other.attribute-name",
            "foreground": "var(color7)"
        },
        {
            "name": "Function call",
            "scope": "variable.function, variable.annotation",
            "foreground": "var(color2)"
        },
        {
            "name": "Library function",
            "scope": "support.function, support.macro",
            "foreground": "var(color2)",
            "font_style": "italic"
        },
        {
            "name": "Library constant",
            "scope": "support.constant",
            "foreground": "var(color7)",
            "font_style": "italic"
        },
        {
            "name": "Library class/type",
            "scope": "support.type, support.class",
            "foreground": "var(color2)",
            "font_style": "italic"
        },
        {
            "name": "Invalid",
            "scope": "invalid",
            "foreground": "var(color1)",
            "background": "var(color10)"
        },
        {
            "name": "Invalid deprecated",
            "scope": "invalid.deprecated",
            "foreground": "var(color1)",
            "background": "var(color12)"
        },
        {
            "name": "YAML Key",
            "scope": "entity.name.tag.yaml",
            "foreground": "var(color8)"
        },
        {
            "name": "YAML String",
            "scope": "source.yaml string.unquoted",
            "foreground": "var(color6)"
        },
        {
            "name": "markup headings",
            "scope": "markup.heading",
            "font_style": "bold"
        },
        {
            "name": "markup headings",
            "scope": "markup.heading punctuation.definition.heading",
            "foreground": "var(color7)"
        },
        {
            "name": "markup h1",
            "scope": "markup.heading.1 punctuation.definition.heading",
            "foreground": "var(color10)"
        },
        {
            "name": "markup links",
            "scope": "string.other.link, markup.underline.link",
            "foreground": "var(color2)"
        },
        {
            "name": "markup bold",
            "scope": "markup.bold",
            "font_style": "bold"
        },
        {
            "name": "markup italic",
            "scope": "markup.italic",
            "font_style": "italic"
        },
        {
            "name": "markup underline",
            "scope": "markup.underline",
            "font_style": "underline"
        },
        {
            "name": "markup bold/italic",
            "scope": "markup.italic markup.bold | markup.bold markup.italic",
            "font_style": "bold italic"
        },
        {
            "name": "markup bold/underline",
            "scope": "markup.underline markup.bold | markup.bold markup.underline",
            "font_style": "bold underline"
        },
        {
            "name": "markup italic/underline",
            "scope": "markup.underline markup.italic | markup.italic markup.underline",
            "font_style": "italic underline"
        },
        {
            "name": "markup bold/italic/underline",
            "scope": "markup.bold markup.italic markup.underline | markup.bold markup.underline markup.italic | markup.italic markup.bold markup.underline | markup.italic markup.underline markup.bold | markup.underline markup.bold markup.italic | markup.underline markup.italic markup.bold",
            "font_style": "bold italic underline"
        },
        {
            "name": "markup hr",
            "scope": "punctuation.definition.thematic-break",
            "foreground": "var(color10)"
        },
        {
            "name": "markup numbered list bullet",
            "scope": "markup.list.numbered.bullet",
            "foreground": "var(color9)"
        },
        {
            "name": "markup blockquote",
            "scope": "markup.quote punctuation.definition.blockquote, markup.list punctuation.definition.list_item",
            "foreground": "var(color10)"
        },
        {
            "name": "markup code",
            "scope": "markup.raw",
            "background": "color(var(color2) alpha(0.38))"
        },
        {
            "name": "markup code",
            "scope": "markup.raw.inline",
            "background": "color(var(color2) alpha(0.5))"
        },
        {
            "name": "markup punctuation",
            "scope": "(text punctuation.definition.italic | text punctuation.definition.bold)",
            "foreground": "var(color7)"
        },
        {
            "name": "diff.header",
            "scope": "meta.diff, meta.diff.header",
            "foreground": "var(color7)"
        },
        {
            "name": "diff.deleted",
            "scope": "markup.deleted",
            "foreground": "var(color10)"
        },
        {
            "name": "diff.inserted",
            "scope": "markup.inserted",
            "foreground": "var(color9)"
        },
        {
            "name": "diff.changed",
            "scope": "markup.changed",
            "foreground": "var(color10)"
        },
        {
            "name": "CSS Properties",
            "scope": "support.type.property-name",
            "foreground": "var(color6)"
        },
        {
            "scope": "constant.numeric.line-number.match",
            "foreground": "var(color10)"
        },
        {
            "scope": "message.error",
            "foreground": "var(color10)"
        },

        {
            "scope": "diff.deleted",
            "background": "hsla(357, 45%, 60%, 0.15)",
            "foreground_adjust": "l(+ 5%)"
        },
        {
            "scope": "diff.deleted.char",
            "background": "hsla(357, 60%, 60%, 0.30)",
            "foreground_adjust": "l(+ 10%)"
        },
        {
            "scope": "diff.inserted",
            "background": "hsla(180, 45%, 60%, 0.15)",
            "foreground_adjust": "l(+ 5%)"
        },
        {
            "scope": "diff.inserted.char",
            "background": "hsla(180, 60%, 60%, 0.30)",
            "foreground_adjust": "l(+ 10%)"
        },
    ]
}
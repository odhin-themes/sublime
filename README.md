# Odhin Theme for [Sublime](https://www.sublimetext.com/)

> A dark theme for [Sublime](https://www.sublimetext.com/).

### HTML
![HTML](https://res.cloudinary.com/odhin/image/upload/v1648461530/projects/odhin-themes/sublime/HTML_tjaxy0.png)

<br>

### PHP
![PHP](https://res.cloudinary.com/odhin/image/upload/v1648461530/projects/odhin-themes/sublime/PHP_wr4flf.png)

<br>

### GO
![GO](https://res.cloudinary.com/odhin/image/upload/v1648461530/projects/odhin-themes/sublime/GOLANG_w5ssp8.png)

<br>

### CSS
![CSS](https://res.cloudinary.com/odhin/image/upload/v1648461530/projects/odhin-themes/sublime/CSS_ktgieg.png)

<br>

### JAVA
![JAVA](https://res.cloudinary.com/odhin/image/upload/v1648461530/projects/odhin-themes/sublime/JAVA_ybtolj.png)

<br>

### RUBY
![RUBY](https://res.cloudinary.com/odhin/image/upload/v1648461530/projects/odhin-themes/sublime/RUBY_vidpzg.png)

<br>

### TYPESCRIPT
![TYPESCRIPT](https://res.cloudinary.com/odhin/image/upload/v1648462961/projects/odhin-themes/sublime/TYPESCRIPT_pyue0z.png)

## Install

#### Install using Package Control

If you are using ```Package Control```, you can easily install Odhin Theme via the Package Control: Install Package menu item. The Odhin Theme package is listed as ```Odhin Color Scheme``` in the packages list.

#### Install using Git

Alternatively, if you are a git user, you can install the theme and keep up to date by cloning the repo directly into your ```Packages``` directory in the Sublime Text application settings area.

You can locate your Sublime Text ```Packages``` directory by using the menu item ```Preferences -> Browse Packages...```.

While inside the ```Packages``` directory, clone the theme repository using the command below:
```
git clone git@gitlab.com:odhin-themes/sublime.git "Odhin Color Scheme"
```

#### Install manually

1.  Download using the [Gitlab .zip download](https://gitlab.com/odhin-themes/sublime/-/archive/master/sublime-master.zip) option
2.  Unzip the files and rename the folder to ```Odhin Color Scheme```
3.  Find your ```Packages``` directory using the menu item ```Preferences -> Browse Packages...```.
4.  Copy the folder into your Sublime Text ```Packages``` directory

#### Activating theme

Run Sublime. The ```Odhin Color Scheme``` will be available from `File -> Preferences -> Select Color Scheme ... -> Odhin` dropdown menu.

## Color Palette

![Colors](https://res.cloudinary.com/odhin/image/upload/v1648462287/projects/odhin-themes/sublime/colors_kyjysh.png)

## License

[MIT License](./LICENSE)
